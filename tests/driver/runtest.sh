#!/bin/sh -ex
 
# Weird location, but okay.
cd ../source
 
# Generate Makefiles, directory layout, ...
dnf -y --nogpgcheck build-dep ../libverto.spec

autoreconf -fiv
./configure --with-glib --with-libevent --disable-dependency-tracking
make -sj
 
# Avoid running `make check` here
cd tests
ls *.c | while read f; do
    f="$(echo $f | awk -F. '{print $1}')"
    make $f
done
cd ..
 
# Overwrite the build.
cd src/.libs
cp /usr/lib64/libverto-glib.so.1.0.0 .
cp /usr/lib64/libverto-libevent.so.1.0.0 .
cd ../..
 
# Go time.
exec make check

